-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

local ntgr = nodecore.tree_growth_rate

function nodecore.tree_growth_rate(pos)
  return ntgr(pos)*10
end

nodecore.register_craft({
		label = "melt cobble to lava",
		action = "cook",
		touchgroups = {flame = 4},
		duration = 30,
		cookfx = true,
		nodes = {
			{
				match = "nc_terrain:cobble",
				replace = "nc_terrain:lava_source"
			}
		}
	})

nodecore.register_cook_abm({nodenames = {"nc_terrain:cobble"}, neighbors = {"group:flame"}})

nodecore.register_limited_abm({
  label = "aggregate insta-cooking",
  interval = 1,
  chance = 1,
  nodenames = {"nc_concrete:wet_source"},
  neighbors = {"group:lava"},
  action = function(pos,node)
    local nnode
    if math.random() > (1/10) then
      nnode = {name = "nc_terrain:hard_stone_1"}
    else
      local t = {"nc_crystal:ore","nc_lux:stone"}
      nnode = {name=t[math.random(1,#t)]}
    end
    minetest.set_node(pos,nnode)
    nodecore.node_sound(pos,"place")
  end
})
