-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, vector
    = minetest, nodecore, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local S = minetest.get_translator(modname)

local alldirs = nodecore.dirs()

local sproutgroups = {
  green = 1,
  sand = 1,
  moist = 1
}

local living = modname .. ":spore"

minetest.register_node(living, {
		description = S"Sponge Spore",
		tiles = {"nc_tree_humus.png^(nc_sponge.png^[opacity:64)^nc_tree_peat.png"},
		groups = {
			crumbly = 2,
      moist = 1,
		},
		sounds = nodecore.sounds("nc_terrain_swishy")
	})

nodecore.register_craft({
		label = "make sponge spore",
		action = "pummel",
		toolgroups = {thumpy = 2},
		nodes = {
			{
				match = {name = "nc_tree:peat", count = 8},
				replace = living
			}
		}
	})

nodecore.register_limited_abm({
		label = "Sponge Spore Sprouting",
		interval = 10,
		chance = 40,
		limited_max = 1000,
		nodenames = {living},
		action = function(pos, node)
      local vdirs = {}
      local opos = pos
      for _,dir in pairs(alldirs) do
        local pos = vector.add(pos,dir)
        local groups = {}
        for k,v in pairs(sproutgroups) do
          groups[k]=0
        end
        for _,dir in pairs(alldirs) do
          local pos = vector.add(pos,dir)
          if not vector.equals(pos,opos) then
            local name = minetest.get_node(pos).name
            for k,v in pairs(sproutgroups) do
              groups[k] = groups[k] + minetest.get_item_group(name,k)
            end
          end
        end
        local valid = true
        for k,v in pairs(groups) do
          if v < sproutgroups[k] then
            valid = false
          end
        end
        if valid and minetest.get_item_group(minetest.get_node(pos).name,"sand") > 0 then
          table.insert(vdirs,dir)
        end
      end
      if #vdirs > 0 then
        local dir = vdirs[math.random(1,#vdirs)]
        local pos = vector.add(pos,dir)
        minetest.set_node(pos,{name="nc_sponge:sponge_living"})
      end
    end
  })
