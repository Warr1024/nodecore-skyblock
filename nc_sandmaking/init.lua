-- LUALOCALS >
local nodecore,minetest =
      nodecore,minetest
-- LUALOCALS <

local modname = minetest.get_current_modname()


minetest.register_node(modname..":dirty_gravel",{
  description = "Dirty Gravel",
  tiles = {"nc_terrain_gravel.png^(nc_terrain_dirt.png^[opacity:127)"},
  groups = {
    crumbly = 1,
    sandmaker = 1
  },
  alternate_loose = {
    groups = {
      crumbly = 2,
      falling_repose = 2
    }
  },
  sounds = nodecore.sounds("nc_terrain_swishy")
})

nodecore.register_craft{
  label = "cook sand from dirty gravel",
  action = "cook",
  touchgroups = {flame=3},
  duration = 30,
  cookfx = true,
  nodes = {
    {
      match = {groups={sandmaker=1}},
      replace = "nc_terrain:sand"
    }
  }
}

nodecore.register_cook_abm({nodenames={"group:sandmaker"},neighbours={"group:flame"}})

nodecore.register_craft{
  label = "mix dirty gravel (fail)",
  action = "pummel",
  toolgroups = {thumpy = 2},
  normal = {y=1},
  priority = 2,
  nodes = {
    {
      match = {groups={dirt=true}},
      replace = "air"
    },
    {
      y=-1,
      x=1,
      match = {buildable_to=true},
      replace = "nc_terrain:gravel"
    },
    {
      y=-1,
      match = {groups={gravel=true}},
      replace = "nc_terrain:dirt"
    }
  }
}

nodecore.register_craft{
  label = "mix dirty gravel",
  action = "pummel",
  toolgroups = {thumpy = 2},
  normal = {y=1},
  priority = 1,
  nodes = {
    {
      match = {groups={dirt=true}},
      replace = "air"
    },
    {
      y=-1,
      match = {groups={gravel=true}},
      replace = modname..":dirty_gravel"
    }
  }
}
